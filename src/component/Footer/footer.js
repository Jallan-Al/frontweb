import './footer.css';
import { BrowserRouter as Router, Link } from "react-router-dom";


function Footer() {

  return (
    <Router>
        
    <footer className="text-center text-lg-start">

        <div className="container p-4">

            <div className="row">

            <div className="col-lg-6 col-md-12 mb-4 mb-md-0">
                <h5 className="text-uppercase">A notre propos</h5>

                <p>
                Le ministère des Solidarités et de la Santé est chargé de la mise en œuvre de la 
                politique du Gouvernement dans les domaines des affaires sociales, de la solidarité 
                et de la cohésion sociale, de la santé publique et de la protection sociale.
                </p>
            </div>

            <div className="col-lg-3 col-md-6 mb-4 mb-md-0">
                <h5 className="text-uppercase">Réseaux Sociaux</h5>

                <ul className="list-unstyled mb-0">
                <li>
                    <a href="#!" className="">Facebook</a>
                </li>
                <li>
                    <a href="#!" className="">Twitter</a>
                </li>
                <li>
                    <a href="#!" className="">Instagram</a>
                </li>
                <li>
                    <a href="#!" className="">Google+</a>
                </li>
                </ul>
            </div>



            <div className="col-lg-3 col-md-6 mb-4 mb-md-0">
                <h5 className="text-uppercase mb-0">Nous contacter</h5>

                <ul className="list-unstyled">
                <li>
                    <a href="#!" className="">tel: (+33) 02 45 78 96 35</a>
                </li>
                <li>
                    <a href="#!" className="">fax: (+33) 02 45 78 96 36</a>
                </li>
                <li>
                    <a href="#!" className="">mél: ministèredelasanteetdelasolidarite@gouv.fr</a>
                </li>
                <li>
                    <a href="#!" className="">Link 4</a>
                </li>
                </ul>
            </div>

            </div>

        </div>



        <div className="text-center p-3">
            © 2020 Copyright:
            <a className="" href=""> ministèredelasanteetdelasolidarite.gouv.fr</a>
        </div>

    </footer>

    </Router>
  );
}

export default Footer;