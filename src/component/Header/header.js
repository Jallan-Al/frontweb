import './header.css';
import { BrowserRouter as Router, Link, Switch, Route } from "react-router-dom";
import { useState } from "react";
import Axios from 'axios';

import Accueil from '../../pages/Accueil/accueil';
import Articles from '../../pages/Articles/articles';
import Quiz from '../../pages/Quiz/quiz';
import Compte from '../../pages/Compte/compte';
import Stat from '../../pages/Stat/Stat';
import AdminAccess from '../../pages/AdminAccess/adminAccess';
import logo from '../../img/Ministere.png'


function Header() {

    const [isConnected, setConnection] = useState(false);
    const [isAdmin, setAdmin] = useState(false);
    const [modalConnexion, setModal] = useState(false);
    const [errorConnexion, setErreur] = useState(false);
    const [modalCreate, setModalCreate] = useState(false);

    const fetchCreateUser=async(login, mdp)=>{
        const response=await Axios.post('https://jsonplaceholder.typicode.com/todos/');   
        alert('Compte créé'); 
    };
    
    function CheckUser() {
        var login;
        var mdp;
        var getlogin;
        var getmdp;
        var getrole;

        login = document.getElementById("login").value;
        mdp = document.getElementById("mdp").value;
        getlogin = 'log';
        getmdp = 'mdp';
        getrole = 'admin';

        if (login === getlogin && mdp === getmdp){
            setConnection(true);
            localStorage.setItem('isConnected', true);
            if (getrole === "admin") {
                localStorage.setItem('adminRole', true);
                setAdmin(true);
            }
            closeModal();
            setErreur(false);
        }else{
            setErreur(true);
        }
    };

    function Disconnect() {
        setConnection(false);
        localStorage.clear();
    };

    function openModal() {
        setModal(true);        
    };

    function closeModal() {
        setModal(false);
    };

    function openModalCreate(){
        setModalCreate(true);
        closeModal();
    };

    function createUser(){
        var inputCreateMdp = document.getElementById('mdpCreate');
        var inputCreateLogin = document.getElementById('loginCreate');
        var createLogin = inputCreateLogin.value;
        var createMdp = inputCreateMdp.value;

        fetchCreateUser(createLogin, createMdp);
    };

    function closeModalCreate(){
        setModalCreate(false);
    };

  return (
    <Router>
        <nav className="navbar navbar-expand-lg navbar-light bg-light" id="header">
            <Link to="/">
                <img src={logo} alt="logo" id="logoheader"></img>
            </Link>
            
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav">
                <li className="nav-item active">
                    <Link className="nav-link" to="/accueil">Accueil<span className="sr-only">(current)</span></Link>
                </li>
                <li className="nav-item active">
                    <Link className="nav-link" to="/articles">Article<span className="sr-only">(current)</span></Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/quiz">Quiz</Link>
                </li>
                
                {isConnected === false && (
                <li className="nav-item" onClick={openModal}><spans className="nav-link" id="btnConnexion">Se connecter</spans></li>
                )}                    


                {isConnected === true && (
                    <li className="nav-item"><Link className="nav-link" to="/mon_compte">mon compte</Link></li>
                )}
                {isAdmin === true && (
                    <li className="nav-item"><Link className="nav-link" to="/statistique">statistique</Link></li>
                )}
                {isConnected === true && (
                    <li className="nav-item" onClick={Disconnect} id="btnDeco"><span className="nav-link" id="btnDeconnexion">deconnexion</span></li>
                )}
                </ul>
            </div>
        </nav>

        {modalConnexion === true && (
            <div className="" id="ConnectionModal">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLabel">Connexion</h5>
                    <button type="button" className="close" onClick={closeModal} aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div className="modal-body">
                    <div className="row">
                    <label className="col-4" htmlFor="login">Identifiant</label>
                    <input className="col-6" type="text" id="login"></input>
                    </div>
                    <div className="row">
                    <label className="col-4" htmlFor="mdp">Mot de passe</label>
                    <input className="col-6" type="password" id="mdp"/>
                    </div>
                    {errorConnexion === true && (
                        <p>Identifiant ou mot de passe incorrect</p>
                    )}
                    <p onClick={openModalCreate}>créer un compte</p>
                </div>
                <div className="modal-footer">
                    <button type="button" className="btn btn-secondary" onClick={closeModal}>Close</button>
                    <button onClick={CheckUser} type="button" className="btn btn-primary">Valider</button>
                </div>
                </div>
            </div>

            </div>
        )}

        {modalCreate === true && (
            <div className="" id="ConnectionModal">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLabel">Inscription</h5>
                    <button type="button" className="close" onClick={closeModalCreate} aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div className="modal-body">
                    <div className="row">
                    <label className="col-4" htmlFor="loginCreate">Identifiant</label>
                    <input className="col-6" type="text" id="loginCreate"></input>
                    </div>
                    <div className="row">
                    <label className="col-4" htmlFor="mdpCreate">Mot de passe</label>
                    <input className="col-6" type="password" id="mdpCreate"/>
                    </div>
                    {errorConnexion === true && (
                        <p>Identifiant ou mot de passe incorrect</p>
                    )}
                </div>
                <div className="modal-footer">
                    <button type="button" className="btn btn-secondary" onClick={closeModalCreate}>Close</button>
                    <button onClick={createUser} type="button" className="btn btn-primary">Valider</button>
                </div>
                </div>
            </div>

            </div>
        )}

        <Switch>
            <Route path="/Accueil">
                <Accueil />
            </Route>
            <Route path="/articles">
                <Articles />
            </Route>
            <Route path="/statistique">
                <Stat />
            </Route>
            <Route path="/quiz">
                <Quiz />
            </Route>
            <Route path="/mon_compte">
                <Compte />
            </Route>
            <Route path="/connexion_su">
                <AdminAccess />
            </Route>
            
        </Switch>
    </Router>
  );
}

export default Header;