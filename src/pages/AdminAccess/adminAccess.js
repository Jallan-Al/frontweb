import './adminAccess.css';
import { BrowserRouter as Router, Link, Switch, Route, Redirect } from "react-router-dom";
import { useState, useEffect } from "react";
import Axios from 'axios';


function AdminAccess() {
const [comments,setComments]=useState([]);
const [user,setUser]=useState('1');
  
useEffect(() => {
	fetchComments();
}, []);
useEffect(() => {
  console.log('==> ',comments)
}, [comments]);

const fetchComments=async()=>{
  const response=await Axios('https://jsonplaceholder.typicode.com/todos/'+user);
  setComments(response.data)    
};

return (
  <div className="App">
    {
      comments.map(comment=>{
        return(
          <div key={comment.id} style={{alignItems:'center',margin:'20px 60px'}}>
          <h4>{comment.name}</h4>//
          <p>{comment.email}</p>
        </div>
        )

      })
    }
    {
      <Route>
          <Redirect to='/accueil' />
      </Route>
      
    }
   
  </div>
);

}

export default AdminAccess;