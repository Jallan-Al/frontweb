import './quiz.css';
import { BrowserRouter as Router, Link } from "react-router-dom";
import { useState, useEffect } from "react";
import axios from 'axios';
import Axios from 'axios';

function Quiz() {
    
    var choix = 1;
    var quiz2 = getgetquiz();

    var [choix, setChoix] = useState(0);
    var [question, setQuestion] = useState(quiz2[0].questionBDD);
    var [proposition1, setProposition1] = useState(quiz2[0].proposition1BDD);
    var [proposition2, setProposition2] = useState(quiz2[0].proposition2BDD);
    var [proposition3, setProposition3] = useState(quiz2[0].proposition3BDD);
    var [proposition4, setProposition4] = useState(quiz2[0].proposition4BDD);
    var [score, setScore] = useState(0);
    var [compteur_question, setCompteur] = useState(0);

    function choixQuiz(choix) {
        setChoix(choix);
    };


    function getgetquiz() {
        var quiz2 = [
            {questionBDD: "aimes tu JCVD ?", proposition1BDD: "oui", proposition2BDD: "dans 20ans l'eau y'en aura plus", proposition3BDD: "non", Proposition4BDD: "c ki", reponseBDD: "dans 20ans l'eau y'en aura plus"},
            {questionBDD: "combien de côté possède un carré ?", proposition1BDD: "2", proposition2BDD: "17", proposition3BDD: "j'ai pas arrêté les maths pour avoir ce genre de question plus tard", Proposition4BDD: "42", reponseBDD: "j'ai pas arrêté les maths pour avoir ce genre de question plus tard"},
            {questionBDD: "si juvabien c'est ?", proposition1BDD: "juvamine", proposition2BDD: "chanmax", proposition3BDD: "que je vais bien", Proposition4BDD: "trop vieux comme référence", reponseBDD: "juvamine"},
            {questionBDD: "un dévellopeur avisé en vaut :", proposition1BDD: "01", proposition2BDD: "10", proposition3BDD: "11", Proposition4BDD: "100", reponseBDD: "10"},
        ];
        return quiz2;
    };
    

    function checkResponse(number, compteur_question, quiz) {
        var reponse = quiz[5][compteur_question]
        var choix = quiz[number][compteur_question]

        if (reponse == choix) {
            setScore(score+1);
            var element = document.getElementById("choix"+number);
            element.classList.add("valide");
        }else{
            var element = document.getElementById("choix"+number);
            element.classList.add("erreur");

            for (var numbers = 1; numbers < 4; numbers++) {
                var choixPossible = quiz[numbers][compteur_question]
                if (reponse == choixPossible) {
                    var elements = document.getElementById("choix"+numbers);
                    elements.classList.add("valide");
                }
            }


        }

    };

    function nextQuestion(compteur_question, quiz) {
        setCompteur(++compteur_question);

        for (var numbers = 1; numbers < 4; numbers++) {
            var element = document.getElementById("choix"+numbers);
            element.classList.remove('valide');
            element.classList.remove('erreur');
        }

        setQuestion(quiz[0][compteur_question]);
        setProposition1(quiz[1][compteur_question]);
        setProposition2(quiz[2][compteur_question]);
        setProposition3(quiz[3][compteur_question]);
        setProposition4(quiz[4][compteur_question]);

        if (quiz[0].compteur_question === undefined) {
            setChoix(-1);
        }
    }

    /////////////////////////////////////
    const [quizs2,setQuizs2]=useState([]);
    const [quiz1,setQuiz1]=useState([]);
    const [step,setStep]=useState(0);
    var [idQuestion,setIdQuestion]=useState(1);

    useEffect(() => {
        fetchQuizs();
    }, []);

    const fetchQuizs=async()=>{
        const response=await Axios('https://jsonplaceholder.typicode.com/comments?postId=1');
        setQuizs2(response.data)    
    };

    const fetchQuiz=async(id)=>{
        const response=await Axios('https://jsonplaceholder.typicode.com/comments?id='+id);
        setQuiz1(response.data)    
    };

    function choixQuiz2(id, idQuestion){
        fetchQuiz(id, idQuestion);//ajout deuxième param pour id question
        setStep(1);
    };

    function checkResponse2(idQuestion, responseUser){
        if(quiz1[0]['name'] === responseUser){
            setScore(score+1);
            var element = document.getElementById("choix"+idQuestion);
            element.classList.add("valide");
        }else{
            var element = document.getElementById("choix"+idQuestion);
            element.classList.add("erreur");
        }
    };

    function nextQuestion2(id, idQuestion){
        fetchQuiz(id, idQuestion);

        if(quiz1 == []){
            setStep(-1);
        }
    }

  return (
    <Router>
        <div className="fond">
        <div id="quizContainer" className="card">
            <h2>Quiz culture G!</h2>
            {/*choix == 0 && (
                <div id="choix_quiz">
                    <p onClick={() => choixQuiz(1)}>quiz 1</p>
                    <p onClick={() => choixQuiz(2)}>quiz 2</p>
                </div>
            )*/}

            {step == 0 && (
                quizs2.map(quiz=>{
                    return(
                    <div key={quiz.id} id="choix_quiz">
                        <p onClick={() => choixQuiz2(quiz.id, 1)}>{quiz.name}</p>
                    </div>
                    )

                })
            )}

            {/*choix > 0 && (
                <div>
                <h3>question :{question} </h3>
                <div>
                    <div className="row">
                        <button className="col-12 col-md-6 choixquiz" id="choix1" onClick={() => checkResponse(1, compteur_question, quiz2)}>
                            {proposition1}
                        </button>
                        <button className="col-12 col-md-6 choixquiz" id="choix2" onClick={() => checkResponse(2, compteur_question, quiz2)}>
                            {proposition2}
                        </button>
                    </div>
                    <div className="row">
                        <button className="col-12 col-md-6 choixquiz" id="choix3" onClick={() => checkResponse(3, compteur_question, quiz2)}>
                            {proposition3}
                        </button>
                        <button className="col-12 col-md-6 choixquiz" id="choix4" onClick={() => checkResponse(4, compteur_question, quiz2)}>
                            {proposition4}
                        </button>
                    </div>
                    <button className="row" id="suivantquiz" onClick={() => nextQuestion(compteur_question, quiz2)}>
                        Question suivante
                    </button>
    
                </div>
                </div>
            )*/} 

            {step > 0 && (
                quiz1.map(quiz=>{
                    return(
                        <div key={quiz.id}>
                            <h3>question :{quiz.body} </h3>
                            <div>
                                <div className="row">
                                    <button className="col-12 col-md-6 choixquiz" id="choix1" onClick={() => checkResponse2(1, quiz.name)}>
                                        {quiz.name}propos1
                                    </button>
                                    <button className="col-12 col-md-6 choixquiz" id="choix2" onClick={() => checkResponse2(2, quiz.name)}>
                                        {quiz.name}propos2
                                    </button>
                                </div>
                                <div className="row">
                                    <button className="col-12 col-md-6 choixquiz" id="choix3" onClick={() => checkResponse2(3, quiz.name)}>
                                        {quiz.name}propos3
                                    </button>
                                    <button className="col-12 col-md-6 choixquiz" id="choix4" onClick={() => checkResponse2(4, quiz.name)}>
                                        {quiz.name}propos4
                                    </button>
                                </div>
                                <button className="row" id="suivantquiz" onClick={() => nextQuestion2(quiz.id, 'idQuiz')}>
                                    Question suivante
                                </button>
                
                            </div>
                        </div>
                    )
            
                  })
            )}

            {step < 0 && (
                <p>quiz terminé, votre score est de {score}</p>
            )}
            
        </div>
        
        </div>
        

    </Router>
  );
}

export default Quiz;