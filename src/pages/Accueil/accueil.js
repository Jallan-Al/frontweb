import './accueil.css';
import { BrowserRouter as Router, Link, Switch, Route } from "react-router-dom";
import { useState, useEffect } from "react";
import Axios from 'axios';



function Accueil() {
    var [isConnected, setConnection] = useState(false);
    var [modalConnexion, setModal] = useState(false);
    var [errorConnexion, setErreur] = useState(false);
    const [bestComments,setBestComments]=useState([]);
    const [newComments,setNewComments]=useState([]);

    useEffect(() => {
      fetchBestComments();
      fetchNewComments();
    }, []);
    
    const fetchBestComments=async()=>{
      const response=await Axios('https://jsonplaceholder.typicode.com/comments?postId=1');
      setBestComments(response.data)    
    };

    const fetchNewComments=async()=>{
      const response=await Axios('https://jsonplaceholder.typicode.com/comments?postId=5');
      setNewComments(response.data)    
    };

  return (
    <Router>
      <div className="row">
        <div className="col-lg-4 col-md-4">
          <div className ="row flex-column p-4" id="aside">
            {
              bestComments.map(comment=>{
                return(
                  <div key={comment.id} className="card m-1 p-1">
                  <h3>{comment.name}</h3>
                  <p>{comment.body}</p>
                  </div>
                )

              })
            }
          </div>
        </div>
        <div className="col-lg-7 col-md-7">
          <div className ="row m-1">
            {
              newComments.map(comment=>{
                return(
                  <div key={comment.id} className="col-lg-3 col-md-8 m-1 card">
                  <h3>{comment.name}</h3>
                  <p>{comment.body}</p>
                  </div>
                )

              })
            }
          </div>
        </div>
      </div>
        
    </Router>
  );
}

export default Accueil;