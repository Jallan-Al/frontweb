import './articles.css';
import { BrowserRouter as Router, Link, Switch, Route } from "react-router-dom";
import { useState, useEffect } from "react";
import Axios from 'axios';


function Articles() {
    

    var [openArticle, setOpnArticle] = useState(false);
    const [articles2, setArticles2]=useState([]);
    const [article2, setArticle2]=useState([]);


    useEffect(() => {
        fetchArticles();
    }, []);

    const fetchArticles=async()=>{
        const response=await Axios('https://jsonplaceholder.typicode.com/comments');
        setArticles2(response.data)    
    };

    const fetchArticle=async(idArticle)=>{
        const response=await Axios('https://jsonplaceholder.typicode.com/comments?id='+idArticle);
        setArticle2(response.data)    
    };

    const delArticle=async(idArticle)=>{
        const response=await Axios.delete('https://jsonplaceholder.typicode.com/posts/'+idArticle);
        alert('Article supprimé');    
    };

    const addComment=async(id, comment)=>{
        const response=await Axios.post('https://jsonplaceholder.typicode.com/posts/'+id);
        alert('commentaire envoyé');    
    };

    function getArticle2(id){
        fetchArticle(id);
        setOpnArticle(true);
    };

    function closeArticle() {
        setOpnArticle(false);
    };

    function deleteArticle(id) {
        delArticle(id);
        setOpnArticle(false);
    };

    function sendNewComment(id){
        var inputNewComment = document.getElementById('inputNewComment');
        var newComment = inputNewComment.nodeValue;
        addComment(id, newComment);
    };

  return (
    <Router>

        {openArticle === false && (
            articles2.map(comment=>{
                return(
                <div key={comment.id} className="card row m-1" onClick={() => getArticle2(comment.id)}>
                    <div  className="col-lg-6 col-md-6 m-1 ">
                        <h3>{comment.name}</h3>
                        <p>{comment.body}</p>
                    </div>
                </div>
                )
            })
        )}
        



        {openArticle === true && (
            article2.map(article2=>{
                return(
            <div className="card openArticle">
            <div onClick={() => closeArticle()}>retour</div>
            <p>{article2.name}</p>
            <p>{article2.body}</p>
            <p>paru le : {article2.id}</p>
            <p>{article2.email}</p>
            <input type="hidden" name={article2.id} id=""/>
            
            {localStorage.getItem('isConnected') === 'true' && (
                <div>
                <div onClick={() => deleteArticle(article2.id)}><p>X</p></div>
                <div ><p>Ajouter au</p></div>
                </div>
            )}
            </div>
            )
        })
        )}

        <div className="card m-4 p-2">
        {openArticle === true && (
            <div>
                <h3 className="mb-4" >Commentaires :</h3>
                <textarea id="inputNewComment" type="textArea" class="form-control col-11 ml-3 mr-4" placeholder="ajouter un commentaire..."></textarea>
                <button onClick={() => sendNewComment(article2.id)} className="btn btn-primary mt-2 ml-4 col-5 offset-2">Envoyer le commentaire</button>
            </div>
        )}
        {openArticle === true && (
            articles2.map(article2=>{
                return(
            <div className="card openArticle">
            <p>{article2.body}</p>
            <p>paru le : {article2.id}</p>
            <p>{article2.email}</p>
            <input type="hidden" name={article2.id} id=""/>
            
            </div>
            )
        })
        )}</div>

        
    </Router>
  );
}

export default Articles;