import './Stat.css';
import { BrowserRouter as Router, Link, Switch, Route } from "react-router-dom";
import { useState, useEffect } from "react";
import Axios from 'axios';



function Stat() {
  const [data,setdata]=useState([]);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData=async()=>{
      const response=await Axios('https://jsonplaceholder.typicode.com/comments?postId='+3);
        setdata(response.data); 
  };

  const fetchDataAsc=async()=>{
    const response=await Axios('https://jsonplaceholder.typicode.com/comments?postId='+1);
      setdata(response.data);
    
  };

  const fetchDataDesc=async()=>{
    const response=await Axios('https://jsonplaceholder.typicode.com/comments?postId='+2);
      setdata(response.data);
  };

  const dataExport=async()=>{
    const response=await Axios.post('https://jsonplaceholder.typicode.com/comments?postId='+2);
      setdata(response.data);
  };

  function sorteAscend() {
    fetchDataAsc();
  };

  function sorteDescend() {
    fetchDataDesc();
  };

  function exportData(id) {
      dataExport();
  };

  return (
    <Router>
        
        <div className="flex-wrap card m-3">
          <table class="table">
            <thead>
              <tr>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Body</th>
              </tr>
            </thead>
            <tbody>
              {
                data.map(data=>{
                  return(
                    <tr>
                      <th scope="row">{data.name}</th>
                      <th>{data.email}</th>
                      <th>{data.body}</th>
                    </tr>
                  )
                })
              }
            </tbody>
          </table>
        </div>
        
        <button className="btn btn-primary m-2" onClick={() => sorteAscend()}>Asc</button>
        <button className="btn btn-primary m-2" onClick={() => sorteDescend()}>Desc</button>
        <button className="btn btn-primary m-2" onClick={() => exportData()}>Export</button>

    </Router>
  );
}

export default Stat;