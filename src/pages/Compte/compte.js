import './compte.css';
import { BrowserRouter as Router, Link, Switch, Route } from "react-router-dom";
import { useState, useEffect } from "react";
import Axios from 'axios';

function Compte() {

    const [messages,setMessages]=useState([]);
    const [message,setMessage]=useState([]);
    const [articles1, setArticles1]=useState([]);
    const [article1, setArticle1]=useState([]);
    const [menu, setMenu]=useState(true);
    const [isOpenArticle1, setOpenArticle1]=useState(false);
    const [isOpenMessage1, setOpenMessage1]=useState(false);
    const [write, setWrite]=useState(false);
    const [repDestinataire, setDestinataire]=useState('');
    const [modify, setModify]=useState(false);

    useEffect(() => {
        fetchArticles();
        fetchMessages();
    }, []);

    const fetchArticles=async()=>{
        const response=await Axios('https://jsonplaceholder.typicode.com/comments?postId=1');
        setArticles1(response.data)    
    };

    const fetchArticle=async(id)=>{
        const response=await Axios('https://jsonplaceholder.typicode.com/comments?id='+id);
        setArticle1(response.data)    
    };

    const fetchMessages=async()=>{
        const response=await Axios('https://jsonplaceholder.typicode.com/comments?postId=2');
        setMessages(response.data)    
    };

    const fetchMessage=async(id)=>{
        const response=await Axios('https://jsonplaceholder.typicode.com/comments?id='+id);
        setMessage(response.data)    
    };

    const delArticle=async(id)=>{
        const response=await Axios.delete('https://jsonplaceholder.typicode.com/posts/'+id);
        alert('Article supprimé');    
    };

    const delMessage=async(id)=>{
        const response=await Axios.delete('https://jsonplaceholder.typicode.com/posts/'+id);
        alert('Message supprimé');    
    };

    const postMessage=async(sujet, destinataire, message)=>{
        const response=await Axios.post('https://jsonplaceholder.typicode.com/posts/');
        alert('Message envoyé');    
    };

    const postModifyArticle=async(titre, corps, id)=>{
        const response=await Axios.post('https://jsonplaceholder.typicode.com/posts/');
        alert('Article modifié');    
    };

    function openMessage1(id){
        setMenu(false);
        fetchMessage(id);
        setOpenMessage1(true);
    };

    function openArticle1(id){
        setMenu(false);
        fetchArticle(id);
        setOpenArticle1(true);
    };

    function writeMessage(destinataire, reponse){
        setWrite(true);
        setMenu(false);

        if (reponse = true){
            setDestinataire(destinataire);
        }
    };

    function backMenu(){
        setMenu(true);
        setOpenMessage1(false);
        setOpenArticle1(false);
        setWrite(false);
        setModify(false);
    };

    function deleteArticle(id){
        delArticle(id);
        setMenu(true);
        setOpenMessage1(false);
        setOpenArticle1(false);
    };

    function deleteMessage(id){
        delMessage(id);
        setMenu(true);
        setOpenMessage1(false);
        setOpenArticle1(false);
    };

    function sendMessage(){
        var sujet = document.getElementById('sujet');
        var destinataireFct = document.getElementById('destinataire');
        var message = document.getElementById('message');

        if (repDestinataire != ''){
            destinataireFct = repDestinataire;
        }else{
            destinataireFct = destinataireFct.value
        }

        postMessage(sujet.value, destinataireFct, message.value);
        backMenu();
    };

    function modifyArticle(id){
        setModify(true);
    };

    function sendModification(id){
        var inputTitre = document.getElementById('titre');
        var inputCorps = document.getElementById('corps');
        
        postModifyArticle(inputTitre.value, inputCorps.value, id);
        backMenu();
    };

  return (
    <Router>
        {menu === true && (
            <div className="row">
                <div className="col-lg-4 col-md-4">
                    <div className ="row flex-column p-4" id="aside">
                        {
                        messages.map(message=>{
                            return(
                            <div key={message.id} className="card m-1 p-1" onClick={() => openMessage1(message.id)}>
                            <h5>{message.name}</h5>
                            <p>{message.body}</p>
                            <p>{message.email}</p>
                            </div>
                            )
                        })
                        }
                        <p className="mt-2" onClick={() => writeMessage('', false)}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                            <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                            <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                            </svg>
                            <span className="m-1 p-1">rédiger un message</span>
                        </p>
                    </div>
                </div>
                <div className="col-lg-7 col-md-7">
                    <div className ="row m-1">
                        {
                        articles1.map(article=>{
                            return(
                            <div key={article.id} className="col-lg-3 col-md-8 m-1 card" onClick={() => openArticle1(article.id)}>
                            <h3>{article.name}</h3>
                            <p>{article.body}</p>
                            </div>
                            )
            
                        })
                        }
                    </div>
                </div>
          </div>
        )}

        {isOpenArticle1 === true && (
        article1.map(article=>{
            return(
                <div key={article.id} className="card m-3 p-3">
                    <h4>{article.name}</h4>
                    <p>corps de l'article{article.body}</p>
                    <p>{article.email}</p>
                    <p className="row">
                        <span className="col-6" onClick={() => backMenu()}>retour</span>
                        <span className="col-3" onClick={() => modifyArticle(article.id)}>modifier</span>
                        <span className="col-3" onClick={() => deleteArticle(article.id)}>supprimer</span>
                    </p>
                </div>
            )
    
          })
          
        )}

        {modify === true && (
            <div className="card m-4 p-3">
                <label htmlFor="titre">Titre</label>
                <input type="text" id="titre"/>

                <label htmlFor="corps">Corps</label>
                <input type="text" id="corps"/>

                <button onClick={() => sendModification(article1[0].id)} className="btn btn-secondary ml-2 mr-2 mt-4">Envoyer</button>
                <button onClick={() => backMenu()} className="btn btn-secondary ml-2 mr-2 mt-2">retour</button> 
            </div>
        )}

        {isOpenMessage1 === true && (
        message.map(message=>{
            return(
                <div key={message.id} className="card m-3 p-3">
                    <h4>{message.name}</h4>
                    <p>{message.body}</p>
                    <p className="row">
                        <span className="col-9">{message.email}</span>
                        <span className="col-3" onClick={() => writeMessage(message.email, true)}>répondre</span>
                    </p>
                    <p className="row">
                        <span className="col-6" onClick={() => backMenu()}>retour</span>
                        <span className="offset-3 col-3"onClick={() => deleteMessage(message.id)}>supprimer</span>
                    </p>
                </div>
            )
    
          })
        )}

        {write === true && (
            <div className="card m-4 p-3">
                <label htmlFor="sujet">sujet</label>
                <input type="text" id="sujet"/>

                <label htmlFor="destinataire">destinataire</label>
                <input type="text" id="destinataire"/>

                <label htmlFor="message">message</label>
                <input type="text" id="message"/>

                <button onClick={() => sendMessage()} className="btn btn-secondary ml-2 mr-2 mt-4">Envoyer</button>
                <button onClick={() => backMenu()} className="btn btn-secondary ml-2 mr-2 mt-2">retour</button>
            </div>
        )}

        
    </Router>
  );
}

export default Compte;